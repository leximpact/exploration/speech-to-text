# Test de transcription automatique de vidéo de l'Assemblée Nationale

Exemple de vidéo : https://videos.assemblee-nationale.fr/video.14696525_65deee7d0cf80.commission-des-finances--proposition-de-loi-visant-a-flecher-l-epargne-non-centralisee-des-livrets--28-fevrier-2024

Télécharger la vidéo sans passer par le système qui demande un mail :
```bash
ffmpeg -i https://anorigin.vodalys.com/videos/definst/mp4/ida/domain1/2024/02/6350_20240228092742.smil/chunklist_b500000.m3u8  -codec copy commission_finances-640x360.mp4
```

Sélection d'un extrait de 00:28:44 à 00:32:44 pour avoir 5 minutes.

Le résultat :
https://www.youtube.com/watch?v=gD2ArhvyFys

Pour 5 minutes de vidéo, il faut :
- `poetry run python 01_extract_with_whispermodel.py` 4 secondes pour extraire l'audio, 1 minute 15 secondes pour la transcription.
- `poetry run python 02_insert_subtitles.py` => 25 minutes ! C'est très long mais ce code utilise un seul coeur de CPU, il est donc grandement optimisable !


OpenAI propose une API payante pour le faire sans avoir besoin d'infrastructure GPU à l'Assemblée.

Sur une vieille carte Nvidia GTX 1080 ti (de 2016), il faut 3 minutes pour 4 minutes de vidéo. La consommation mémoire vidéo est de moins de 4 Go.

Observations :
- Certaines expressions sont mal retranscrites, comme "gresser des pattes" qui devient "greffer l'EHPAD". Ou "ça surprend" devient "s'assure point".
- Les "bafouillage" ne sont pas restranscrits.
- Il est possible de générer la transcription avec des timecodes (temps dans la vidéo), ce qui permet de développer un outil de correction avec l'audio ou encore du sous-titres automatique de vidéo.
- Pas de détection du locuteur, mais c'est possible en le combinant avec un modèle de détection de locuteur tel que https://huggingface.co/pyannote/speaker-diarization-3.1.

Problème d'installation, voir https://github.com/SYSTRAN/faster-whisper/issues/516 qui propose la solution suivante :
`export LD_LIBRARY_PATH=${PWD}/.venv/lib/python3.10/site-packages/nvidia/cublas/lib:${PWD}/.venv/lib/python3.10/site-packages/nvidia/cudnn/lib`

Ou plus automatisé : `export LD_LIBRARY_PATH=`python3 -c 'import os; import nvidia.cublas.lib; import nvidia.cudnn.lib; print(os.path.dirname(nvidia.cublas.lib.__file__) + ":" + os.path.dirname(nvidia.cudnn.lib.__file__))'``

Liens utils :
- https://huggingface.co/bofenghuang/whisper-large-v3-french
- https://github.com/m-bain/whisperX
- https://github.com/SYSTRAN/faster-whisper/tree/master
- https://www.digitalocean.com/community/tutorials/how-to-generate-and-add-subtitles-to-videos-using-python-openai-whisper-and-ffmpeg
