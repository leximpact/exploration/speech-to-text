"""
This script converts a video file to an audio file and then transcribes the audio file to JSON text using the whisper model.
Thanks to https://github.com/ramsrigouthamg/Supertranslate.ai/tree/main/Scrolling_Subtitles_On_Video_using_Python
"""

import ffmpeg
from dotenv import load_dotenv
from datetime import datetime
import os
import json
if load_dotenv():
    print("Environment variables loaded")
    print(f"{os.environ['HF_HOME']=}")
    from faster_whisper import WhisperModel


print(f"{datetime.now()} Start...")

videofilename = "/media/2To-nvme/dev/speech-to-text/datasets/commission_finance_extrait-5min.mp4"
audiofilename = videofilename.replace(".mp4",'.mp3')

# Create the ffmpeg input stream
input_stream = ffmpeg.input(videofilename)

# Extract the audio stream from the input stream
audio = input_stream.audio

# Save the audio stream as an MP3 file
output_stream = ffmpeg.output(audio, audiofilename)

print (audiofilename)
# Overwrite output file if it already exists
output_stream = ffmpeg.overwrite_output(output_stream)

ffmpeg.run(output_stream)

print(f"{datetime.now()} Audio extracted.")

model_size = "medium"
# expected one of: tiny.en, tiny, base.en, base, small.en, small, medium.en, medium, large-v1, large-v2, large-v3, large, distil-large-v2, distil-medium.en, distil-small.en
model_size = "large-v3"
model = WhisperModel(model_size, device="cuda")
# model = WhisperModel(model_size, device="cpu", compute_type="int8")

segments, info = model.transcribe(audiofilename, word_timestamps=True)
segments = list(segments)  # The transcription will actually run here.
# for segment in segments:
#     for word in segment.words:
#         print("[%.2fs -> %.2fs] %s" % (word.start, word.end, word.word))

wordlevel_info = []

for segment in segments:
    for word in segment.words:
      wordlevel_info.append({'word':word.word,'start':word.start,'end':word.end})


with open(videofilename.replace(".mp4",'.json'), 'w') as f:
    json.dump(wordlevel_info, f,indent=4)

print(f"{datetime.now()} End...")