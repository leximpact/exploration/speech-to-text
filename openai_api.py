from dotenv import load_dotenv
if load_dotenv():
    from  openai import OpenAI
client = OpenAI()

audio_file = open("/media/2To-nvme/dev/speech-to-text/datasets/commission_finance_extrait.mp3", "rb")
transcript = client.audio.transcriptions.create(
  file=audio_file,
  model="whisper-1",
  response_format="verbose_json",
  timestamp_granularities=["word"]
)

print(transcript.words)
with open("output-openai.txt", "w") as f:
    f.write(transcript.words)