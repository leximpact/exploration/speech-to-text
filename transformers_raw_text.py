"""
Based on https://huggingface.co/bofenghuang/whisper-large-v3-french

This script uses the whisper-large-v3-french to extract the text from an audio file.
The text has no timestamps.

"""
from dotenv import load_dotenv
from datetime import datetime
import os
if load_dotenv():
    print("Environment variables loaded")
    print(f"{os.environ['HF_HOME']=}")
    import torch
    from transformers import AutoModelForSpeechSeq2Seq, AutoProcessor, pipeline

# Set device and dtype
device = "cuda:0" if torch.cuda.is_available() else "cpu"
print(f"Device: {device}")
torch_dtype = torch.float16 if torch.cuda.is_available() else torch.float32

# Load model
model_name_or_path = "bofenghuang/whisper-large-v3-french"
processor = AutoProcessor.from_pretrained(model_name_or_path)
model = AutoModelForSpeechSeq2Seq.from_pretrained(
    model_name_or_path,
    torch_dtype=torch_dtype,
    low_cpu_mem_usage=True,
)
model.to(device)

# Init pipeline
pipe = pipeline(
    "automatic-speech-recognition",
    model=model,
    feature_extractor=processor.feature_extractor,
    tokenizer=processor.tokenizer,
    torch_dtype=torch_dtype,
    device=device,
    chunk_length_s=10,  # for long-form transcription, missing part with value above 10 !
    max_new_tokens=128,
)

# Run pipeline
print(f"{datetime.now()} Running pipeline...")
result = pipe("/media/2To-nvme/dev/speech-to-text/datasets/commission_finance_extrait.mp3")  # return_timestamps=True does nothing
print(f"{datetime.now()} End pipeline...")

with open("output.txt", "w") as f:
    f.write(result["text"])
print("Saved to output.txt")
